﻿using ExchangeRates.Models;
using Newtonsoft.Json;
using System;

namespace ExchangeRates.Data
{
    public class HttpSerializer
    {
        public static ExchangeTable SerializeExchangeTablesAsync(string data)
        {
            ExchangeTable getData = new ExchangeTable();
            try
            {
                getData = JsonConvert.DeserializeObject<ExchangeTable>(data);
            }
            catch (Exception ex)
            {
                var a = ex.Message.ToString();

            }
            return getData;
        }


    }
}