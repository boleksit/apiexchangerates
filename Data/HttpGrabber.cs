﻿using ApiExchangeRates.Data;
using ApiExchangeRates.Models;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeRates.Data
{
    public class HttpGrabber
    {
        private static string baseAddres = "http://api.nbp.pl/api/exchangerates/";


        public static async Task<string> GetTablesAsync(string table)
        {
            string answear = "";
            LogModel log = new LogModel();
            LogDataContext logDataContext = new LogDataContext();
            try
            {
                var request = HttpWebRequest.CreateHttp(baseAddres + "tables/" + table + "/");
                request.Method = WebRequestMethods.Http.Get;
                request.ContentType = "application/json";
                log.RequestTime = DateTime.Now;
                log.Request = request.ToString();
                await Task.Factory.FromAsync<WebResponse>(request.BeginGetResponse, request.EndGetResponse, null)
                    .ContinueWith(task =>
                    {
                        var response = (HttpWebResponse)task.Result;

                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            StreamReader responseReader = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
                            string responseData = responseReader.ReadToEnd();

                            answear = responseData.ToString().Substring(1, responseData.ToString().Length - 2);
                            responseReader.Close();
                            log.ResponseTime = DateTime.Now;
                            log.Response = answear;
                            log.Type = "NBP";

                            logDataContext.Log.Add(log);
                            logDataContext.SaveChanges();

                        }

                        response.Close();
                    });

            }
            catch (Exception ex)
            {
                var a = ex.Message.ToString();
            }


            return answear;
        }

    }
}