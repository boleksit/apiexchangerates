﻿using ApiExchangeRates.Models;
using Microsoft.EntityFrameworkCore;

namespace ApiExchangeRates.Data
{
    public class LogDataContext : DbContext
    {
        public DbSet<LogModel> Log { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("server=(localdb)\\MSSQLLocalDB;database=testdb;Trusted_Connection=true");
        }


    }
}
