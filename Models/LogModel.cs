﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ApiExchangeRates.Models
{
    [Table("Log", Schema = "dbo")]
    public class LogModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [Column(TypeName = "datetime")]
        [Display(Name = "Request Time")]
        public DateTime RequestTime { get; set; }

        [NotMapped]
        public HttpContext Context { get; set; }

        [Required]
        //[Column(TypeName = "string")]
        [Display(Name = "Type")]
        public string Type { get; set; }
        [Required]
        //[Column(TypeName = "string")]
        [Display(Name = "Request")]
        public string Request { get; set; }

        [Required]
        [Column(TypeName = "datetime")]
        [Display(Name = "Response Time")]
        public DateTime ResponseTime { get; set; }

        [Required]
        // [Column(TypeName = "string")]
        [Display(Name = "Response")]
        public string Response { get; set; }
    }

}
