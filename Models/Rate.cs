﻿namespace ExchangeRates.Models
{

    public class Rate
    {
        public string currency { get; set; }
        public string code { get; set; }

        public Rate(string _currency, string _code)
        {
            currency = _currency;
            code = _code;
        }
        public Rate()
        {

        }

    }


}