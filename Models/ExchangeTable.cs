﻿using System.Collections.Generic;

namespace ExchangeRates.Models
{
    public class ExchangeTable
    {
        public string table { get; set; }
        public string no { get; set; }
        public string tradingDate { get; set; }
        public string effectiveDate { get; set; }
        public List<ExchangeRate> rates { get; set; }
    }
}