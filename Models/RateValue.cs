﻿using ExchangeRates.Models;

namespace ApiExchangeRates.Models
{
    public class RateValue : Rate
    {
        public double value { get; set; }

        public RateValue(string _currency, string _code, double _value)
        {
            currency = _currency;
            code = _code;
            value = _value;

        }
    }
}
