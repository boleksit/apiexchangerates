﻿namespace ExchangeRates.Models
{
    public class ExchangeRate : Rate
    {
        public double bid { get; set; }
        public double ask { get; set; }

        public ExchangeRate()
        {

        }

        public ExchangeRate(string _code)
        {
            code = _code;
        }
    }



}