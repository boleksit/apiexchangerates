﻿using ApiExchangeRates.Models;
using ExchangeRates.Data;
using ExchangeRates.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApiExchangeRates.Controllers
{
    public class ExchangeController : ControllerBase
    {

        /// <summary>
        ///  Get Currencies table with exchange rates
        /// </summary>

        [HttpGet("api/exchangetable")]
        public async Task<ExchangeTable> Get()
        {
            return HttpSerializer.SerializeExchangeTablesAsync(await HttpGrabber.GetTablesAsync("C"));
        }

        // GET api/exchangetable/list 
        /// <summary>
        /// Get list of availible currencies for exchange
        /// </summary>
        /// <returns></returns>
        [HttpGet("api/exchangetable/list")]
        public async Task<List<Rate>> GetExchangeList()
        {
            List<Rate> response = new List<Rate>();
            response.Add(new Rate("polski złoty", "PLN"));
            foreach (Rate item in HttpSerializer.SerializeExchangeTablesAsync(await HttpGrabber.GetTablesAsync("C")).rates)
            {
                response.Add(new Rate(item.currency, item.code));
            }
            return response;
        }

        // GET api/exchangetable/USD
        /// <summary>
        /// Get exchange rate for specific currency
        /// </summary>
        /// <param name="currency">Currency code i.e: USD</param>
        /// <returns></returns>
        [HttpGet("api/exchangetable/currency/{currency}")]
        public async Task<ExchangeRate> Get(string currency)
        {
            ExchangeTable exchangeTable = HttpSerializer.SerializeExchangeTablesAsync(await HttpGrabber.GetTablesAsync("C"));
            ExchangeRate exchangeRate = new ExchangeRate();

            for (int i = 0; i < exchangeTable.rates.Count; i++)
            {
                if (exchangeTable.rates[i].code.ToUpper() == currency.ToUpper())
                {
                    exchangeRate = exchangeTable.rates[i];
                    break;
                }
            }
            return exchangeRate;
        }

        //GET api/exchange?from=USD&to=PLN&value=1
        /// <summary>
        /// Get exchange betwen two currencies
        /// </summary>
        /// <param name="from">Source currency i.e: USD</param>
        /// <param name="to">Target currency i.e: AUD</param>
        /// <param name="value">Value to exchange i.e: 1</param>
        /// <returns></returns>
        [HttpGet("api/exchange")]
        public async Task<RateValue> Get([FromQuery]string from, [FromQuery]string to, [FromQuery]double value)
        {
            ExchangeTable exchangeTable = HttpSerializer.SerializeExchangeTablesAsync(await HttpGrabber.GetTablesAsync("C"));
            ExchangeRate exchangeRateFrom = new ExchangeRate();
            ExchangeRate exchangeRateTo = new ExchangeRate();
            for (int i = 0; i < exchangeTable.rates.Count; i++)
            {
                if (exchangeTable.rates[i].code.ToUpper() == from.ToUpper())
                {
                    exchangeRateFrom = exchangeTable.rates[i];
                }

                if (exchangeTable.rates[i].code.ToUpper() == to.ToUpper())
                {
                    exchangeRateTo = exchangeTable.rates[i];
                }
            }
            if (from.ToUpper() == "PLN")
            {
                return new RateValue(exchangeRateTo.currency, exchangeRateTo.code, value * exchangeRateTo.bid);
            }

            if (to.ToUpper() == "PLN")
            {
                return new RateValue("polski złoty", "PLN", value / exchangeRateFrom.ask);
            }
            else
            {
                return new RateValue(exchangeRateTo.currency, exchangeRateTo.code, value * exchangeRateFrom.ask / exchangeRateTo.bid);
            }
        }
        //GET api/exchangetable/USD,AUD
        /// <summary>
        /// Get exchange rate for list of currencies
        /// </summary>
        /// <param name="list">List of currencies i.e: USD,AUD</param>
        /// <returns></returns>
        [HttpGet("api/exchangetable/{list}")]
        public async Task<List<ExchangeRate>> GetList(string list)
        {
            List<ExchangeRate> result = new List<ExchangeRate>();
            string[] dataSplited;
            dataSplited = list.Split(',');


            ExchangeTable exchangeTable = HttpSerializer.SerializeExchangeTablesAsync(await HttpGrabber.GetTablesAsync("C"));



            foreach (ExchangeRate item in exchangeTable.rates)
            {
                foreach (string incomeItem in dataSplited)
                {
                    if (incomeItem.ToUpper() == item.code.ToUpper())
                    {
                        result.Add(item);
                    }
                }
            }
            return result;
        }

    }
}
